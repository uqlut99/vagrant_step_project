#!/bin/bash
JAR_NAME=$(ls /home/petuser/ | grep .jar)

if ps aux | grep ${JAR_NAME} | grep -v grep ; then
        echo "App is running" >> /dev/null
else
        java -jar /home/petuser/${JAR_NAME} --spring.profiles.active=mysql
fi          